$(document).ready(function(){

	// AGREGANDO CLASE ACTIVE AL PRIMER ENLACE ====================
	$('.category_list .category_item[category="all"]').addClass('ct_item-active');

	// FILTRANDO PRODUCTOS  ============================================

	$('.category_item').click(function(){
		var catProduct = $(this).attr('category');
		console.log(catProduct);

		// AGREGANDO CLASE ACTIVE AL ENLACE SELECCIONADO
		$('.category_item').removeClass('ct_item-active');
		$(this).addClass('ct_item-active');

		// OCULTANDO PRODUCTOS =========================
		$('.product-item').css('transform', 'scale(0)');
		function hideProduct(){
			$('.product-item').hide();
		} setTimeout(hideProduct,400);

		// MOSTRANDO PRODUCTOS =========================
		function showProduct(){
			$('.product-item[category="'+catProduct+'"]').show();
			$('.product-item[category="'+catProduct+'"]').css('transform', 'scale(1)');
		} setTimeout(showProduct,400);
	});

	// MOSTRANDO TODOS LOS PRODUCTOS =======================

	$('.category_item[category="all"]').click(function(){
		function showAll(){
			$('.product-item').show();
			$('.product-item').css('transform', 'scale(1)');
		} setTimeout(showAll,400);
	});
});
// jquery
$(document).ready(function(){

	var acercade=$('#tienda1').offset().top,
	contrato1=$('#contrato1').offset().top,
	registro1=$('#registro11').offset().top;
	

	$('#tienda0').on('click',function(e){
		e.preventDefault();
		$('html,body').animate({
			scrollTop:acercade -100

		},500);
	});
	$('#contrato0').on('click',function(e){
		e.preventDefault();
		$('html,body').animate({
			scrollTop:contrato1 -100

		},500);
	});
	$('#registro0').on('click',function(e){
		e.preventDefault();
		$('html,body').animate({
			scrollTop:registro1 -100

		},500);
	});

});



