-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-05-2019 a las 21:13:39
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ver`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_admin` int(11) NOT NULL,
  `nombre_admin` varchar(45) DEFAULT NULL,
  `apellido_admin` varchar(45) DEFAULT NULL,
  `telefono_admin` int(11) DEFAULT NULL,
  `correo_admin` varchar(45) DEFAULT NULL,
  `contrasena_admin` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_admin`, `nombre_admin`, `apellido_admin`, `telefono_admin`, `correo_admin`, `contrasena_admin`) VALUES
(1, 'Jose', 'peña', 68471920, 'jose@gmail.com', '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `id_compra` int(11) NOT NULL,
  `fecha_compra` date NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `cantidad_compra` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`id_compra`, `fecha_compra`, `precio_compra`, `cantidad_compra`, `id_usuario`, `id_producto`) VALUES
(10, '2019-04-22', 0, 0, 0, 0),
(15, '2019-05-11', 10, 1, 3, 1),
(16, '2019-05-11', 4, 0, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `categoria_producto` varchar(250) NOT NULL,
  `nombre_producto` varchar(250) NOT NULL,
  `precio_producto` varchar(250) NOT NULL,
  `cantidad_producto` int(30) NOT NULL,
  `imagen_producto` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `categoria_producto`, `nombre_producto`, `precio_producto`, `cantidad_producto`, `imagen_producto`) VALUES
(12, 'verduras', 'Piedra Pulida', '10', 50, 'img/Piedra-pulida.jpg'),
(13, 'verduras', 'Piedra  Decorativa', '6', 50, 'img/piedra decorativa.jpg'),
(14, 'Frutas', 'Tierra Amarilla', '5', 30, 'img/tierra.jpg'),
(15, 'verduras', 'Tierra Negra', '4', 15, 'img/tierra negra.jpg'),
(17, 'Especias', 'fina ', '4', 15, 'img/tierra.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `id_trabajdor` int(11) NOT NULL,
  `nombre_trabajdor` varchar(45) NOT NULL,
  `apellido_trabajador` varchar(45) NOT NULL,
  `telefono_trabajador` int(11) NOT NULL,
  `correo_trabajador` varchar(45) NOT NULL,
  `contrasena_trabajador` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajdor`
--

CREATE TABLE `trabajdor` (
  `id_trabajdor` int(11) NOT NULL,
  `nombre_trabajdor` varchar(45) NOT NULL,
  `apellido_trabajador` varchar(45) NOT NULL,
  `telefono_trabajador` int(11) NOT NULL,
  `correo_trabajador` varchar(45) NOT NULL,
  `contrasena_trabajador` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(45) DEFAULT NULL,
  `apellido_usuario` varchar(45) DEFAULT NULL,
  `telefono_usuario` int(11) DEFAULT NULL,
  `correo_usuario` varchar(45) DEFAULT NULL,
  `contrasena_usuario` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre_usuario`, `apellido_usuario`, `telefono_usuario`, `correo_usuario`, `contrasena_usuario`) VALUES
(2, 'jp', 'peÃ±a', 77452089, 'jp@gmail.com', '123'),
(3, 'jp', 'peÃ±a', 77452089, 'jp@gmail.com', '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id_compra`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`id_trabajdor`);

--
-- Indices de la tabla `trabajdor`
--
ALTER TABLE `trabajdor`
  ADD PRIMARY KEY (`id_trabajdor`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  MODIFY `id_trabajdor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajdor`
--
ALTER TABLE `trabajdor`
  MODIFY `id_trabajdor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
